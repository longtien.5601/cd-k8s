#!/bin/bash

# File path of the Deployment YAML

sed -i "s|image:.*|image: $DOCKER_USER/$IMAGE_NAME:$IMAGE_TAG|" deployment.yaml
sed -i "s|replicas:.*|replicas: $REPLICAS|" deployment.yaml

echo "Image updated to $DOCKER_USER/$IMAGE_NAME:$IMAGE_TAG"


